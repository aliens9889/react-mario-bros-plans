# Mario Bros Plans

This React project is a light twitter (well that's how I see it) as user you will add projects and add some details about it, besides this project will handle authentication with Firebase (I think is awesome!).

# Important

The original project is by **The Net Ninja** you can check his Youtube's channel [here](https://www.youtube.com/channel/UCW5YeuERMmlnqo4oq8vwUpg) and you can find so much more that you feel interested.

Why this part is considerate important? Because if you want to follow the original tutorial, you can check **The Net Ninja** github's project [here](https://github.com/iamshaunjp/React-Redux-Firebase-App). There is something that some people on his youtube's channel comments about this project, that they (as me at beginning) were following the tutorial step by step, video by video and don't considerate that the Firebase version would be an important step to move forward with the tutorial.

At first I was thinking that probably I was doing something wrong or probably I missed something in the process, but then, after suffered for a little while lol. I found out (searching on Internet) that the last version on Firebase (V3) has changed some things in compare with Firebase V2. In my mind I was saying: Ok, that means you have two options:

- One. If you want to continue with the tutorial you have to install the dependencies with the same version like **The Net Ninja** tutorial and his gihub's repository has it. This option is good, and will be a reminder if you follow the next tutorial from him or other tutorial, that be aware with the dependencies and versions of x projects.

- Two. Keep the project and move forward with all current dependencies, but you have to figure it out how to make work the project.

So I thought: Ok **The Net Ninja** already has the project with Firebase version 2 (V2). I could do the same project but with the Firebase next's version (V3). That way me and others developers can get more info about how to handle this project with the most recent changes. So I took it as a personal challenge (it was hard, but I think this second option was the best decision for me).

Once you have this very very clear, then we can move on.

What I like about this project is it's handle different elements that would be helpful for other project that you could have in mind or this will good as guide for next projects. Elements like: Redux, React Router DOM, React Redux Firebase, Redux Firestore, Redux Thunk (I know probably is a lot, but it will worth it to practice and learn new stuff)

I will work with **Yarn** to install dependencies and run the project. Feel free if you prefer work with **NPM** it is good too.

If the is another tips that I think is important to everyone or as reminder to myself I will add it here (That's with you Alonso, don't be lazy when you considerate something important add it on Readme, ok? Ok, don't yelling me lol).
